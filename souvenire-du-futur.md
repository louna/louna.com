# NOV 2019  
- prépares ton retour en France
- 17h Santiago petit bar resto Julian petite rue
- Rodriguez patron - salsa
- Asso organise des manifestation féministe
- Affiches, évènements, la fête du clitoris
- 5 à 8 amis - on bosse ensemble
- 4 dans la collocation et 1 allemande et 1 espagnol (gay)
- 1/5 ème de ton salaire
- Je suis en fringues décontractées
- Je n'ai pas de relation amoureuse claire
- Je parle comme si c'était ma langue natale
- Contente : on rigole, bonne soirée, vous avez réussi un projet
- Triste : Ma famille me manque, surtout mon frère, Morgane est repartie
- Peur : de pas retrouver les choses que j'ai laissé
- Colère : Plus on est dans le mouvement féministe plus on voit les injustices. Une amie vient de vivre quelque chose de dur.

# Aout 2019
- Je viens de rentrer dans la colloc
- Morgane vient d'arriver on prévoit un roadtrip
- Je m'acclimate à la colloc
- Je visite Santiago avec Morgane
- Du bruit, ça crie
- Je reparle français, ça fait bizarre
- Ça sent pas super bon, ça sent la vie, des enfants, des gens
- Peur : Que Morgane fasse chier, que ça se passe mal avec l'asso
- Joie : Morgane, ça fait des mois que j'attends
- Triste : La voir me fait me rendre compte que les gens me manquent
- Colère : Pas
- Je vais de me faire embaucher, et je vais faire mon roadtrip avant


# Mai 2019
- Dans un village, accueillit par une famille
- Je vais dans une autre ferme
- Je commence à bien parler, je sais me faire comprendre
- Mon arrivée est loin dans ma tête, j'ai fait tellement de trucs
- skype, FaceTime, what app, slack
- Pas super à l'aise, savoir si je vais tenir le coup
- J'apprécie mais c'est encore dur
- J'ai mis des habits à moi
- Chèvre, chevaux. Pantalon de cheval et pull basique
- Je monte à cheval des fois
- Triste : Le manque arrive, loin des personnes que j'aime
- Colère : Contre moi, j'arrive pas à apprécier ce que je vie, trop concentrer sur la vie d'avant
- Joie : Un petit poulain est né, les gens de la famille sont sympas et je me sens en sécurité
- Inquiète : J'arrive pas encore à trouver une asso
