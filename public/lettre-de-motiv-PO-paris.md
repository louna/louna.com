Madame, Monsieur,

En réponse à la fiche du poste de Business Developper à la DGCCRF.

Après lecture de votre offre, je souhaite vous informer que je suis motivée par le stage que vous proposez.

J'y vois l'opportunité de solidifier mes compétences en terme d'accompagnement de projet.

Déjà sensibilisée par les approches agile et lean, je suis plutôt sur l'idée d'apporter de la valeur au produit qu'attirée par les démarches commerciales ou marketing dans lesquelles à la fois mes compétences sont réduites mais l'interêt que j'y porte aussi.  
Ma vision : placer l'utilisat·eur·rice·s au centre et fabriquer un produit qui répond à un besoin.  

À 16 ans, j'ai co-créé [Plein Phare](http://pleinphare.xyz/), un outil d'accompagnement pour les lycéens face aux problématiques d'orientation en utilisant l'approche lean startup.
Dans ce projet construit à partir d'interviews de lycéen·ne·s, nous avons beaucoup travaillé sur les valeurs et l'intention à l'aide d'ateliers agiles et collaboratifs.

Les compétences demandées pour ce stage me parlent beaucoup :

- Travail d'équipe : j'ai pour ainsi dire toujours travaillé en équipe. "Tout seul on va plus vite ensemble on va ailleurs" est devenu un de mes mantras autant pour plein phare que pendant mon service civique d'un an dans une association de lutte contre les discriminations où j'ai pu faire l'expérience d'un binôme très harmonieux lors d'interventions, mais aussi d'une collaboraton avec quelqu'un qui n'avait ni la même culture, ni les mêmes valeurs, ni la même façon de travailler. Cela fut dur mais très instructif. J'ai pu ainsi travailler ma patience ainsi que la communication non violente.

- Relationnel : De part un de mes traits de caractère principal : l'empathie, je peux dire que j'ai un très bon sens du relationnel, très à l'aise avec les mots et la parole, en langage courant on dit souvent que "Je mets les gens à l'aise". J'ai une qualité d'écoute forte qui me permet de bien prendre en compte les besoins exprimés ou sous-entendus.

- Communication : Sensibilisée à la communication non violente et à l'écoute empathique, la communication avec l'autre a peu de secret pour moi. La nécessité d'une posture basse dans le rapport à l'autre est pour moi évident et cela me facilite beaucoup à la fois dans mes projets et ma vie quotidienne.

- Organiser des ateliers : Lors de mon service civique je me suis étonnée seule de mon aisance à prendre la parole en public et à gérer un groupe, certe les thèmes étaient plus profonds (sexisme, racisme, immigration, handicap), mais nos outils ludiques me font dire qu'une fois qu'on a été animatrice, la compétence à cette organisation est là.

Autonome et à l'écoute, je fais de mon jeune âge un atout et non un frein. Je suis persuadée que ma place est parmi vous, quand j'entends "Si vous souhaitez faire changer les choses et construire un Etat doté d'outils pour faire face aux défis du XXI ème siècel", ma fougue et mon dynamisme s'emportent et j'entends alors "Louna voilà une possibilité de te développer et de continuer à changer les choses. Et devine quoi ? Tu n'as même pas à t'exclure du système", voilà ce qui me porte et qui me donne envie d'avancer, qui me dit qu'on a besoin de moi quelque part pour faire changer les choses.
